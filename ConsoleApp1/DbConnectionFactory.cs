using System;
using System.Data.Common;
using Npgsql;

namespace ConsoleApp1
{
    public class DbConnectionFactory : IDbConnectionFactory
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly PostgresConfig _config;
        private readonly string _connectionString;

        public DbConnectionFactory(PostgresConfig config)
        {
            _config = config;
            _connectionString =
                $"Host={_config.Host};Username={_config.Username};Password={_config.Password};Database={_config.Database};Port={_config.Port}";
        }

        public IPgsqlConnection GetNewConnection()
        {
            Logger.Info("Getting a new database connection.");
            PgsqlConnection conn = new PgsqlConnection(new NpgsqlConnection(_connectionString));
            conn.Open();
            return conn;
        }
    }
}