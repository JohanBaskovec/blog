namespace ConsoleApp1
{
    public interface IHttpRequest
    {
        string RequestBody { get; }
        string RawUrl { get; }
        string Method { get; }
        string AbsolutePath { get; }
        string GetStringParam(string key);
    }
}