namespace ConsoleApp1
{
    public interface IErrorController
    {
        void Error404(IHttpRequest request, IJsonResponse response);
        void ErrorMethodNotAllowed(IHttpRequest request, IJsonResponse response);
    }
}