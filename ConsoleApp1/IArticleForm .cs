namespace ConsoleApp1
{
    public interface IArticleForm
    {
        string Title { get; set; }
        string Content { get; set; }
        bool Validate();
        IArticle CreateArticle();
        string ToString();
        void UpdateArticle(IArticle article);
    }
}