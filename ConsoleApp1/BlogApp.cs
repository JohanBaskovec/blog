using System;
using System.Data.Common;
using Npgsql;

namespace ConsoleApp1
{
    /**
     * Web app that uses a Postgres database.
     */
    public class BlogApp
    {
        private readonly IArticleController _articleController;
        private readonly IErrorController _errorController;
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly IArticleRepositoryFactory _articleRepositoryFactory;
        private readonly IArticleFormFactory _articleFormFactory;

        public BlogApp(
            IArticleController articleController,
            IArticleRepositoryFactory articleRepositoryFactory,
            IErrorController errorController,
            IDbConnectionFactory dbConnectionFactory,
            IArticleFormFactory articleFormFactory
        )
        {
            _articleController = articleController;
            _articleRepositoryFactory = articleRepositoryFactory;
            _errorController = errorController;
            _dbConnectionFactory = dbConnectionFactory;
            _articleFormFactory = articleFormFactory;
        }

        public void Route(IHttpRequest request, IJsonResponse response)
        {
            string[] urlParts = request.AbsolutePath.Split("/");
            string controller = "";
            string idString = "";
            long? id = null;

            if (urlParts.Length >= 2)
            {
                controller = urlParts[1];
            }

            if (urlParts.Length >= 3)
            {
                idString = urlParts[2];
                if (idString.Length > 0)
                {
                    try
                    {
                        id = long.Parse(idString);
                    }
                    catch (Exception e)
                    {
                        response.JsonMap["error"] = "Invalid id.";
                        response.Send(400);
                        return;
                    }
                }
            }

            Console.WriteLine(controller);
            Console.WriteLine(idString);
            Console.WriteLine($"Received request on URL {request.RawUrl}");
            Console.WriteLine($"Routing request to {controller}/{idString}");
            try
            {
                switch (controller)
                {
                    case "article":
                    {
                        RouteToArticleController(request, response, id);
                        break;
                    }
                    default:
                        _errorController.Error404(request, response);
                        break;
                }
            }
            catch (Exception e)
            {
                Console.Write(e.ToString());
                response.JsonMap["error"] = "Oops, an exception occured that wasn't caught by a controller!";
                response.Send(500);
            }
        }

        private void RouteToArticleController(IHttpRequest request, IJsonResponse response, long? id)
        {
            switch (request.Method)
            {
                case "GET":
                    using (IPgsqlConnection dbConnection = _dbConnectionFactory.GetNewConnection())
                    {
                        ArticleRepository articleRepository = _articleRepositoryFactory.Make(dbConnection);
                        _articleController.Get(request, response, articleRepository, id);
                    }

                    break;
                case "POST":
                    using (IPgsqlConnection dbConnection = _dbConnectionFactory.GetNewConnection())
                    {
                        ArticleRepository articleRepository = _articleRepositoryFactory.Make(dbConnection);
                        _articleController.Post(request, response, articleRepository, _articleFormFactory, id);
                    }

                    break;
                default:
                    _errorController.ErrorMethodNotAllowed(request, response);
                    break;
            }
        }
    }
}