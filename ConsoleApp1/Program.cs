﻿using System;
using System.ComponentModel;
using System.Data.Common;
using System.IO;
using System.Threading.Tasks.Sources;
using CommandLine;
using NLog;
using Npgsql;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace ConsoleApp1
{
    static class Program
    {
        // initialized in InitLogging
        private static NLog.Logger Logger = null;
        private static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
                .WithParsed(options => Run(options));
        }

        private static void Run(Options options)
        {
            InitLogging(options.Env);
            AppConfig appConfig = GetAppConfig();
            DbConnectionFactory dbConnectionFactory = InitDatabase(options.ResetDatabase, appConfig.Postgres);

            var articleRepositoryFactory = new ArticleRepositoryFactory();
            var articleController = new ArticleController();
            var errorController = new ErrorController();
            var articleFormFactory = new ArticleFormFactory();

            var blogApp = new BlogApp(
                articleController,
                articleRepositoryFactory,
                errorController,
                dbConnectionFactory,
                articleFormFactory
            );
            var server = new Server(8085, blogApp);
            server.Start();
        }

        private static AppConfig GetAppConfig()
        {
            using (var sr = new StreamReader("AppConfig.yml"))
            {
                Deserializer deserializer = new DeserializerBuilder()
                    .WithNamingConvention(new CamelCaseNamingConvention())
                    .Build();
                var appConfig = deserializer.Deserialize<AppConfig>(sr);
                Logger.Info("Appconfig: {0}", appConfig);
                return appConfig;
            }
        }

        private static DbConnectionFactory InitDatabase(bool reset, PostgresConfig config)
        {
            var dbConnectionFactory = new DbConnectionFactory(config);

            if (reset)
            {
                ResetDatabase(dbConnectionFactory);
            }

            return dbConnectionFactory;
        }

        private static void InitLogging(string optionsMode)
        {
            switch (optionsMode)
            {
                case "development":
                    NLog.LogManager.LoadConfiguration("NLogDev.config");
                    break;
                case "production":
                    NLog.LogManager.LoadConfiguration("NLogProd.config");
                    break;
                default:
                    throw new Exception("Invalid environment");
            }

            Logger = NLog.LogManager.GetCurrentClassLogger();
        }

        private static void ResetDatabase(DbConnectionFactory dbConnectionFactory)
        {
            Console.WriteLine("Resetting the database");
            using (IPgsqlConnection conn = dbConnectionFactory.GetNewConnection())
            {
                Console.WriteLine("Dropping tables.");
                ExecuteFile(conn, "blog_drop.sql");
                Console.WriteLine("Recreating tables.");
                ExecuteFile(conn, "blog.sql");
                Console.WriteLine("Inserting test data.");
                ExecuteFile(conn, "blog_test_data.sql");
            }
        }

        private static void ExecuteFile(IPgsqlConnection conn, string filePath)
        {
            string text = System.IO.File.ReadAllText(filePath, System.Text.Encoding.UTF8);
            using (DbCommand command = conn.CreateCommand())
            {
                command.CommandText = text;
                command.ExecuteNonQuery();
            }
        }
    }
}