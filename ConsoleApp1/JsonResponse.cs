using System.Collections.Generic;

namespace ConsoleApp1
{
    public interface IJsonResponse
    {
        int Code { get; }
        bool Sent { get; }
        void Send(int code);
        Dictionary<string, object> JsonMap { get; set; }
    }

    public class JsonResponse : IJsonResponse
    {
        public int Code { get; private set; }
        public bool Sent { get; private set; }
        public Dictionary<string, object> JsonMap { get; set; } = new Dictionary<string, object>();

        public void Send(int code)
        {
            this.Code = code;
            this.Sent = true;
        }

    }
}