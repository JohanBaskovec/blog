using System.Data.Common;
using Npgsql;

namespace ConsoleApp1
{
    public interface IDbConnectionFactory
    {
        IPgsqlConnection GetNewConnection();
    }
}