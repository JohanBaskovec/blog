namespace ConsoleApp1
{
    public class AppConfig
    {
        public PostgresConfig Postgres { get; set; }

        public override string ToString()
        {
            return $"{nameof(Postgres)}: {Postgres}";
        }
    }
}