using System.Collections.Generic;
using System.Data.Common;
using System.Resources;
using Npgsql;
using NpgsqlTypes;

namespace ConsoleApp1
{
    public class ArticleRepository : IArticleRepository
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly IPgsqlConnection _connection;

        public ArticleRepository()
        {
        }
        
        public ArticleRepository(IPgsqlConnection connection)
        {
            _connection = connection;
        }

        public IEnumerable<IArticle> GetAll()
        {
            Logger.Info("Getting all articles.");
            var ret = new List<IArticle>();
            using (DbCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "select id, title, content from article";
                cmd.Prepare();
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    IArticle article = CreateArticleFromDbRow(reader);
                    ret.Add(article);
                }
            }

            return ret;
        }

        public IArticle CreateArticleFromDbRow(DbDataReader reader)
        {
            var article = new Article(
                id: reader.GetInt64(0),
                title: reader.GetString(1),
                content: reader.GetString(2)
            );
            if (Logger.IsDebugEnabled)
            {
                Logger.Trace("Creating article: {0}", article);
            }

            return article;
        }

        public IArticle GetById(long id)
        {
            Logger.Info($"Getting article with id {id}");
            using (NpgsqlCommand cmd = _connection.CreateCommand())
            {
                cmd.CommandText = "select id, title, content from article where id=@id";
                cmd.Parameters.AddWithValue("id", NpgsqlDbType.Integer, id);
                cmd.Prepare();
                using (DbDataReader reader = cmd.ExecuteReader())
                {
                    if (!reader.Read())
                    {
                        Logger.Trace($"Found no article with id {id}");
                        return null;
                    }

                    IArticle article = CreateArticleFromDbRow(reader);
                    Logger.Trace($"Got article {article}");
                    return article;
                }
            }
        }

        public void Save(IArticle article)
        {
            if (article.Id == 0)
            {
                Logger.Info($"Saving new article");
                if (Logger.IsTraceEnabled)
                {
                    Logger.Trace(article.ToString);
                }

                using (NpgsqlCommand cmd = new NpgsqlCommand())
                {
                    cmd.Connection = _connection.NpgsqlConnection;
                    cmd.CommandText = "insert into article (title, content) values(@title, @content)";
                    cmd.Parameters.AddWithValue("title", NpgsqlDbType.Text, article.Title);
                    cmd.Parameters.AddWithValue("content", NpgsqlDbType.Text, article.Content);
                    cmd.Prepare();
                    cmd.ExecuteNonQuery();
                }
            }
            else
            {
                Logger.Info($"Saving existing article");
                if (Logger.IsTraceEnabled)
                {
                    Logger.Trace(article.ToString);
                }

                using (NpgsqlCommand cmd = new NpgsqlCommand())
                {
                    cmd.Connection = _connection.NpgsqlConnection;
                    cmd.CommandText = "update article set title=@title, content=@content where id=@id";
                    cmd.Parameters.AddWithValue("title", NpgsqlDbType.Text, article.Title);
                    cmd.Parameters.AddWithValue("content", NpgsqlDbType.Text, article.Content);
                    cmd.Parameters.AddWithValue("id", NpgsqlDbType.Integer, article.Id);
                    cmd.Prepare();
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}