using Newtonsoft.Json;

namespace ConsoleApp1
{
    public class Json
    {
        public static string SerializeObject(object o)
        {
            return JsonConvert.SerializeObject(o);
        }

        public static T DeserializeObject<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}