using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json;

namespace ConsoleApp1
{
    /**
     * Single threaded HTTP REST server
     */
    public class Server
    {
        private int Port { get; }
        
        private HttpListener listener;
        private ErrorController _errorController;
        private BlogApp App { get; }

        public Server(int port, BlogApp app)
        {
            Port = port;
            App = app;
        }

        /**
         * Wait for HTTP request, create an HttpResponse and HttpRequest
         * objects that are passed to the App's route method,
         * which will set response code, headers, json response content etc.,
         * and send the response back to client.
         */
        public void Start()
        {
            Console.WriteLine($"Server started on port {Port}");
            listener = new HttpListener();
            listener.Prefixes.Add($"http://*:{Port}/");
            listener.Start();
            while (true)
            {
                HttpListenerContext context = listener.GetContext();
                HttpListenerRequest dotNetRequest = context.Request;
                HttpListenerResponse dotNetResponse = context.Response;

                var response = new JsonResponse();
                var request = new HttpRequest(dotNetRequest);
                App.Route(request, response);
                string responseString = JsonConvert.SerializeObject(response.JsonMap);
                byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
                dotNetResponse.ContentLength64 = buffer.Length;
                using (Stream output = dotNetResponse.OutputStream)
                {
                    output.Write(buffer, 0, buffer.Length);
                }
            }
        }

    }
}