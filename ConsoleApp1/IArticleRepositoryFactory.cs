using System.Data.Common;
using Npgsql;

namespace ConsoleApp1
{
    public interface IArticleRepositoryFactory
    {
        ArticleRepository Make(IPgsqlConnection connection);
    }
}