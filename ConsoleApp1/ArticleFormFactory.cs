namespace ConsoleApp1
{
    public class ArticleFormFactory : IArticleFormFactory
    {
        public IArticleForm FromJson(string json)
        {
            return Json.DeserializeObject<ArticleForm>(json);
        }
    }
}