using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Net.Security;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using Npgsql;
using Npgsql.TypeMapping;
using IsolationLevel = System.Data.IsolationLevel;

namespace ConsoleApp1
{
    public interface IPgsqlConnection : IDisposable
    {
        object GetLifetimeService();
        object InitializeLifetimeService();
        void Dispose();
        IContainer Container { get; }
        ISite Site { get; set; }
        INpgsqlTypeMapper TypeMapper { get; }
        string ConnectionString { get; set; }
        string Host { get; }
        int Port { get; }
        int ConnectionTimeout { get; }
        int CommandTimeout { get; }
        string Database { get; }
        string DataSource { get; }
        bool IntegratedSecurity { get; }
        string UserName { get; }
        ConnectionState FullState { get; }
        ConnectionState State { get; }
        ProvideClientCertificatesCallback ProvideClientCertificatesCallback { get; set; }
        RemoteCertificateValidationCallback UserCertificateValidationCallback { get; set; }
        Version PostgreSqlVersion { get; }
        string ServerVersion { get; }
        int ProcessID { get; }
        bool HasIntegerDateTimes { get; }
        string Timezone { get; }
        IReadOnlyDictionary<string, string> PostgresParameters { get; }
        event EventHandler Disposed;
        Task OpenAsync();
        event StateChangeEventHandler StateChange;
        void Open();
        Task OpenAsync(CancellationToken cancellationToken);
        NpgsqlCommand CreateCommand();
        NpgsqlTransaction BeginTransaction();
        NpgsqlTransaction BeginTransaction(IsolationLevel level);
        void EnlistTransaction(Transaction transaction);
        void Close();
        NpgsqlBinaryImporter BeginBinaryImport(string copyFromCommand);
        NpgsqlBinaryExporter BeginBinaryExport(string copyToCommand);
        TextWriter BeginTextImport(string copyFromCommand);
        TextReader BeginTextExport(string copyToCommand);
        NpgsqlRawCopyStream BeginRawBinaryCopy(string copyCommand);
        void MapEnum<TEnum>(string pgName = null, INpgsqlNameTranslator nameTranslator = null) where TEnum : struct;
        void MapComposite<T>(string pgName = null, INpgsqlNameTranslator nameTranslator = null) where T : new();
        bool Wait(int timeout);
        bool Wait(TimeSpan timeout);
        void Wait();
        Task WaitAsync(CancellationToken cancellationToken);
        Task WaitAsync();
        DataTable GetSchema();
        DataTable GetSchema(string collectionName);
        DataTable GetSchema(string collectionName, string[] restrictions);
        void ChangeDatabase(string dbName);
        void UnprepareAll();
        void ReloadTypes();
        event NoticeEventHandler Notice;
        event NotificationEventHandler Notification;
        NpgsqlConnection NpgsqlConnection { get; }
    }
}