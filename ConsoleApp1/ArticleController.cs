using System;

namespace ConsoleApp1
{
    public class ArticleController : IArticleController
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public void Get(
            IHttpRequest request,
            IJsonResponse response,
            IArticleRepository articleRepository,
            long? id
        )
        {
            if (id == null)
            {
                Logger.Info("Getting all articles.");
                response.JsonMap["values"] = articleRepository.GetAll();
            }
            else
            {
                Logger.Info($"Getting article with id {id}");
                IArticle article = articleRepository.GetById(id.Value);
                if (article == null)
                {
                    response.JsonMap["error"] = "Article not found.";
                }
                else
                {
                    response.JsonMap["value"] = article;
                }
            }

            response.Send(200);
        }

        public void Post(
            IHttpRequest request,
            IJsonResponse response,
            IArticleRepository articleRepository,
            IArticleFormFactory articleFormFactory,
            long? id
        )
        {
            IArticleForm form = articleFormFactory.FromJson(request.RequestBody);
            if (form.Validate())
            {
                IArticle article = null;
                if (id == null)
                {
                    article = form.CreateArticle();

                    Logger.Info($"Creating new article {article}");
                    articleRepository.Save(article);
                }
                else
                {
                    article = articleRepository.GetById(id.Value);
                    if (article == null)
                    {
                        Logger.Info($"Tried to update article with id {id}, but it doesn't exist.");
                        // todo : generate error map
                        response.JsonMap["error"] = $"Article {id} not found.";
                        response.Send(400);
                        return;
                    }

                    form.UpdateArticle(article);
                    articleRepository.Save(article);
                }

                response.JsonMap["id"] = article.Id;
                response.JsonMap["title"] = article.Title;
                response.JsonMap["content"] = article.Content;
            }
            else
            {
                Logger.Info($"Received invalid form {form}");
                // todo : generate error map
                response.JsonMap["error"] = "Invalid form.";
            }
        }
    }
}