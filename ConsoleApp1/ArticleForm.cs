namespace ConsoleApp1
{
    public class ArticleForm : IArticleForm
    {
        public string Title { get; set; }
        public string Content { get; set; }

        public ArticleForm(string title, string content)
        {
            Title = title;
            Content = content;
        }

        public bool Validate()
        {
            return Title != null && Content != null;
        }

        public IArticle CreateArticle()
        {
            var article = new Article(Title, Content);
            return article;
        }

        public void UpdateArticle(IArticle article)
        {
            article.Content = Content;
            article.Title = Title;
        }

        public override string ToString()
        {
            return $"Title: {Title}, Content: {Content}";
        }
    }
}