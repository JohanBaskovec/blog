namespace ConsoleApp1
{
    public interface IArticle
    {
        long Id { get; set; }
        string Title { get; set; }
        string Content { get; set; }
        string ToString();
    }
}