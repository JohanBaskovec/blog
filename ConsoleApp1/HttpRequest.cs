using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;

namespace ConsoleApp1
{

    public class HttpRequest : IHttpRequest
    {
        private readonly HttpListenerRequest _dotNetRequest;
        private string _requestBody;

        public string AbsolutePath => _dotNetRequest.Url.AbsolutePath;

        public string RequestBody
        {
            get
            {
                if (_requestBody == null)
                {
                    Stream stream = _dotNetRequest.InputStream;
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        _requestBody = reader.ReadToEnd();
                    }
                }

                return _requestBody;
            }
        }

        public string RawUrl => _dotNetRequest.RawUrl;
        public string Method => _dotNetRequest.HttpMethod;
        //public NameValueCollection QueryString => dotNetRequest.QueryString;
        
        public string GetStringParam(string key)
        {
            return _dotNetRequest.QueryString[key];
        }

        public HttpRequest()
        {
        }

        public HttpRequest(HttpListenerRequest dotNetRequest)
        {
            this._dotNetRequest = dotNetRequest;
        }
    }
}