namespace ConsoleApp1
{
    public interface IArticleFormFactory
    {
        IArticleForm FromJson(string json);
    }
}