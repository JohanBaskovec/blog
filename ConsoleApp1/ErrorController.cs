namespace ConsoleApp1
{
    public class ErrorController : IErrorController
    {
        public void Error404(IHttpRequest request, IJsonResponse response)
        {
            response.JsonMap["error"] = $"Not found.";
        }

        public void ErrorMethodNotAllowed(IHttpRequest request, IJsonResponse response)
        {
            response.JsonMap["error"] = $"Method {request.Method} not allowed.";
        }
    }
}