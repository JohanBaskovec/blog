namespace ConsoleApp1
{
    public interface IArticleController
    {
        void Get(
            IHttpRequest request,
            IJsonResponse response,
            IArticleRepository articleRepository,
            long? id
        );

        void Post(
            IHttpRequest request,
            IJsonResponse response,
            IArticleRepository articleRepository,
            IArticleFormFactory articleFormFactory,
            long? id
        );
    }
}