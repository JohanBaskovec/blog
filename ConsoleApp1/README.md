## Development
* install Jetbrains Rider
* install PostgreSQL 9.6
* create a user named 'postgres' if he doesn't already exist
* create a database called 'blog'
* copy NLogDev.config, all .sql files and AppConfig.yml to wherever your binaries will be built (by default in bin/debug/netcoreapp2.1)
* change the postgres password in AppConfig.yml
* edit the Default run configuration, set the program arguments to:
```
--env=development --reset-database=true
```
* run the tests by going in the unit tests tab, select the top level project and click run
* run the server, it will drop all tables, recreate them, insert some fake data and listen on port 8085

