namespace ConsoleApp1
{
    public class PostgresConfig
    {
        public string Host { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Database { get; set; }
        public int Port { get; set; }

        public override string ToString()
        {
            return
                $"{nameof(Host)}: {Host}, {nameof(Username)}: {Username}, {nameof(Password)}: {Password}, {nameof(Database)}: {Database}, {nameof(Port)}: {Port}";
        }
    }
}