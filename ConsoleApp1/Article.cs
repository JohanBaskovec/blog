namespace ConsoleApp1
{
    public class Article: IArticle
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public Article(string title = "", string content = "", long id = 0)
        {
            Id = id;
            Title = title;
            Content = content;
        }

        public override string ToString()
        {
            return $"Id: {Id}, Title: {Title}, Content: {Content}";
        }
    }
}