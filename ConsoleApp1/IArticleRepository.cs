using System.Collections.Generic;
using System.Data.Common;
using Npgsql;

namespace ConsoleApp1
{
    public interface IArticleRepository
    {
        IEnumerable<IArticle> GetAll();
        IArticle CreateArticleFromDbRow(DbDataReader reader);
        IArticle GetById(long id);
        void Save(IArticle article);
    }
}

