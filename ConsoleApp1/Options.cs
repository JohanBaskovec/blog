using CommandLine;
using Newtonsoft.Json;

namespace ConsoleApp1
{
    public class Options
    {
        [Option('e', "env", Required = true, HelpText = "Env can either be \"development\" or \"production\".")]
        public string Env { get; set; }
        
        [Option('r', "reset-database", Required = false, HelpText = "Set to true to reset the database with test data.")]
        public bool ResetDatabase { get; set; }
    }
}