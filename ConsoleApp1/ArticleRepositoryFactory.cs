using Npgsql;

namespace ConsoleApp1
{
    public class ArticleRepositoryFactory : IArticleRepositoryFactory
    {
        public ArticleRepository Make(IPgsqlConnection connection)
        {
            return new ArticleRepository(connection);
        }
    }
}