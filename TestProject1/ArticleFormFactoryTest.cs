using ConsoleApp1;
using Xunit;

namespace TestProject1
{
    public class ArticleFormFactoryTest
    {
        [Fact]
        public void FromJson_ShouldReturnValidFormWhenJsonIsValid()
        {
            string json = @"
{
    ""title"": ""test title"",
    ""content"": ""test content""
}
";
            var factory = new ArticleFormFactory();
            IArticleForm form = factory.FromJson(json);
            Assert.Equal("test title", form.Title);
            Assert.Equal("test content", form.Content);
        }
        
        [Fact]
        public void FromJson_ShouldReturnFormWithNullFieldsWhenJsonIsEmptyObject()
        {
            string json = @"
{
}
";
            var factory = new ArticleFormFactory();
            IArticleForm form = factory.FromJson(json);
            Assert.Null(form.Title);
            Assert.Null(form.Content);
        }
    }
}