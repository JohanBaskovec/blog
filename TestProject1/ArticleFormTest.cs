using ConsoleApp1;
using Xunit;

namespace TestProject1
{
    public class ArticleFormTest
    {
        [Fact]
        public void Validate_ShouldReturnTrueWhenFormIsValid()
        {
            var form = new ArticleForm("Test", "Test");
            Assert.True(form.Validate());
        }
        
        [Fact]
        public void Validate_ShouldReturnFalseWhenTitleIsNull()
        {
            var form = new ArticleForm(null, "Test");
            Assert.False(form.Validate());
        }
        
        [Fact]
        public void Validate_ShouldReturnFalseWhenContentIsNull()
        {
            
            var form = new ArticleForm("Test", null);
            Assert.False(form.Validate());
        }
        
        [Fact]
        public void Validate_ShouldReturnFalseWhenContentAndTitleAreNull()
        {
            
            var form = new ArticleForm(null, null);
            Assert.False(form.Validate());
        }
    }
}