using System.Collections.Generic;
using ConsoleApp1;
using Moq;
using Npgsql;
using Xunit;

namespace TestProject1
{
    public class BlogAppTest
    {
        private BlogApp _blogApp;
        private readonly Mock<IArticleController> _articleController;
        private readonly Mock<IErrorController> _errorController;
        private readonly Mock<IDbConnectionFactory> _dbConnectionFactory;
        private readonly Mock<IArticleRepositoryFactory> _articleRepositoryFactory;
        private readonly Mock<IArticleFormFactory> _articleFormFactory;
        private readonly Mock<IHttpRequest> _httpRequest;
        private readonly Mock<IJsonResponse> _jsonResponse;

        public BlogAppTest()
        {
            _articleController = new Mock<IArticleController>();
            _errorController = new Mock<IErrorController>();
            _dbConnectionFactory = new Mock<IDbConnectionFactory>();
            _articleRepositoryFactory = new Mock<IArticleRepositoryFactory>();
            _articleFormFactory = new Mock<IArticleFormFactory>();
            _httpRequest = new Mock<IHttpRequest>();
            _jsonResponse = new Mock<IJsonResponse>();
            
            _blogApp = new BlogApp(
                _articleController.Object,
                _articleRepositoryFactory.Object,
                _errorController.Object,
                _dbConnectionFactory.Object,
                _articleFormFactory.Object
            );
        }

        
        [Fact]
        public void Route_ShouldCallArticleControllerGetMethodWithoutId()
        {
            _jsonResponse.Setup(m => m.JsonMap).Returns(new Dictionary<string, object>());
            var conn = new Mock<IPgsqlConnection>();
            var articleRepository = new Mock<ArticleRepository>();
            _httpRequest.Setup(m => m.AbsolutePath).Returns("/article/");
            _httpRequest.Setup(m => m.Method).Returns("GET");
            _dbConnectionFactory.Setup(m => m.GetNewConnection()).Returns(conn.Object);
            _articleRepositoryFactory.Setup(m => m.Make(conn.Object)).Returns(articleRepository.Object);
            
            _blogApp.Route(_httpRequest.Object, _jsonResponse.Object);
            
            _dbConnectionFactory.Verify(m => m.GetNewConnection(), Times.Once);
            _articleRepositoryFactory.Verify(m => m.Make(conn.Object), Times.Once);
            _articleController.Verify(m => m.Get(_httpRequest.Object, _jsonResponse.Object, articleRepository.Object, null), Times.Once);
        }
        
        [Fact]
        public void Route_ShouldCallArticleControllerGetMethodWithId()
        {
            _jsonResponse.Setup(m => m.JsonMap).Returns(new Dictionary<string, object>());
            var conn = new Mock<IPgsqlConnection>();
            var articleRepository = new Mock<ArticleRepository>();
            _httpRequest.Setup(m => m.AbsolutePath).Returns("/article/54");
            _httpRequest.Setup(m => m.Method).Returns("GET");
            _dbConnectionFactory.Setup(m => m.GetNewConnection()).Returns(conn.Object);
            _articleRepositoryFactory.Setup(m => m.Make(conn.Object)).Returns(articleRepository.Object);
            
            _blogApp.Route(_httpRequest.Object, _jsonResponse.Object);
            
            _dbConnectionFactory.Verify(m => m.GetNewConnection(), Times.Once);
            _articleRepositoryFactory.Verify(m => m.Make(conn.Object), Times.Once);
            _articleController.Verify(m => m.Get(_httpRequest.Object, _jsonResponse.Object, articleRepository.Object, 54), Times.Once);
        }
    }
}