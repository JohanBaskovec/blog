using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using ConsoleApp1;
using Moq;
using Xunit;

namespace TestProject1
{
    public class ArticleControllerTest
    {
        private ArticleController _articleController;
        private Mock<IHttpRequest> _request;
        private Mock<IJsonResponse> _response;
        private Mock<IArticleRepository> _articleRepository;
        private Mock<IArticleFormFactory> _articleFormFactory;

        public ArticleControllerTest()
        {
            _articleController = new ArticleController();
            _request = new Mock<IHttpRequest>();
            _response = new Mock<IJsonResponse>();
            _response.Setup(mock => mock.JsonMap).Returns(new Dictionary<string, object>());
            _articleRepository = new Mock<IArticleRepository>();
            _articleFormFactory = new Mock<IArticleFormFactory>();
        }

        [Fact]
        public void GivenThatThereAreArticlesInDb_GetWithoutIdParamShouldReturnAllArticles()
        {
            var articles = new List<Article> {new Article(), new Article(), new Article()};
            _request.Setup(mock => mock.GetStringParam(It.Is<string>(arg => arg == "id"))).Returns((string) null);
            _articleRepository.Setup(mock => mock.GetAll()).Returns(articles);

            _articleController.Get(_request.Object, _response.Object, _articleRepository.Object, null);

            Assert.Equal(articles, _response.Object.JsonMap["values"]);
        }

        [Fact]
        public void GivenThatThereAreArticlesInDb_GetWithIdParamOfArticleShouldReturnThatArticle()
        {
            var article = new Article();
            article.Id = 53;
            _articleRepository.Setup(mock => mock.GetById(It.Is<long>(it => it == 53))).Returns(article);

            _articleController.Get(_request.Object, _response.Object, _articleRepository.Object, 53);

            Assert.Equal(article, _response.Object.JsonMap["value"]);
        }

        [Fact]
        public void GivenThatValidArticleFormIsReceived_PostShouldSaveArticle()
        {
            var articleForm = new Mock<IArticleForm>();
            var article = new Article("Test title", "Test content", 42);
            articleForm.Setup(a => a.Validate()).Returns(true);
            articleForm.Setup(a => a.CreateArticle()).Returns(article);

            _articleFormFactory.Setup(m => m.FromJson(It.IsAny<string>())).Returns(articleForm.Object);

            _articleController.Post(
                _request.Object,
                _response.Object,
                _articleRepository.Object,
                _articleFormFactory.Object,
                null
            );

            articleForm.Verify(a => a.Validate(), Times.Once);
            articleForm.Verify(a => a.CreateArticle(), Times.Once);
            _articleRepository.Verify(a => a.Save(article), Times.Once);
            Assert.Equal(_response.Object.JsonMap["id"], article.Id);
            Assert.Equal(_response.Object.JsonMap["title"], article.Title);
            Assert.Equal(_response.Object.JsonMap["content"], article.Content);
        }

        [Fact]
        public void GivenThatInvalidArticleFormIsReceived_PostShouldNotSaveArticleAndReturnErrorMessage()
        {
            var articleForm = new Mock<IArticleForm>();
            articleForm.Setup(a => a.Validate()).Returns(false);

            _articleFormFactory.Setup(m => m.FromJson(It.IsAny<string>())).Returns(articleForm.Object);

            _articleController.Post(
                _request.Object,
                _response.Object,
                _articleRepository.Object,
                _articleFormFactory.Object,
                null
            );

            articleForm.Verify(a => a.Validate(), Times.Once);
            articleForm.Verify(a => a.CreateArticle(), Times.Never);
            _articleRepository.Verify(a => a.Save(It.IsAny<IArticle>()), Times.Never);
            Assert.NotNull(_response.Object.JsonMap["error"]);
        }
        
        [Fact]
        public void Post_ShouldUpdateExistingArticleWhenValidFormIsReceived()
        {
            var articleForm = new Mock<IArticleForm>();
            var article = new Article("Test title", "Test content", 42);
            _articleFormFactory.Setup(m => m.FromJson(It.IsAny<string>())).Returns(articleForm.Object);
            articleForm.Setup(a => a.Validate()).Returns(true);
            _articleRepository.Setup(a => a.GetById(42)).Returns(article);


            _articleController.Post(
                _request.Object,
                _response.Object,
                _articleRepository.Object,
                _articleFormFactory.Object,
                42
            );

            articleForm.Verify(a => a.Validate(), Times.Once);
            _articleRepository.Verify(a => a.GetById(42), Times.Once);
            _articleRepository.Verify(a => a.Save(article), Times.Once);
            Assert.Equal(_response.Object.JsonMap["id"], article.Id);
            Assert.Equal(_response.Object.JsonMap["title"], article.Title);
            Assert.Equal(_response.Object.JsonMap["content"], article.Content);
        }
    }
}